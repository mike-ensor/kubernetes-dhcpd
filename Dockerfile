FROM debian:12-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y smbclient iputils-ping wget curl cifs-utils psmisc winbind nano

RUN apt-get -q -y update \
 && apt-get -q -y -o "DPkg::Options::=--force-confold" -o "DPkg::Options::=--force-confdef" install apt-utils \
 && rm -rf /etc/dpkg/dpkg.cfg.d/excludes \
 && apt-get -q -y -o "DPkg::Options::=--force-confold" -o "DPkg::Options::=--force-confdef" install dumb-init isc-dhcp-server man iproute2 \
 && apt-get -q -y autoremove \
 && apt-get -q -y clean \
 && rm -rf /var/lib/apt/lists/*

USER root
WORKDIR /

RUN useradd dhcpd && mkdir /data

COPY ./run.sh /run.sh
COPY ./run-dhcp.sh /run-dhcp.sh
COPY ./dhcpd.leases /data/dhcpd.leases

RUN chown dhcpd:dhcpd /data && chown dhcpd:dhcpd /*.sh && chmod 666 /data/dhcpd.leases

ENTRYPOINT ["/run.sh"]