###
### 1. Copy this template and name it `.envrc`
### 2. Replace the values as needed to match your project and docker repo
### 3. Run `direnv allow` to load the environment variables (if using direnv, else `source .envrc`)
###
### This project was built using Google tools and repository in mind. Adjust as needed to match your environment.
###

# Project ID
export PROJECT_ID="sample-google-project-id"

# Docker repository host name
export REPO_HOST="gcr.io"

# Folder in Docker repo
export REPO_FOLDER="dhcp-server"
