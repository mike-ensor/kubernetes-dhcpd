#!/bin/bash

### This is a shell to run the DHCP Server in a given time period

## $1 is the interface to run the DHCP Server on
## JOB_TTL is the max time to run the server in seconds

TTL=${JOB_TTL:-300} # default to 5 minutes (300 seconds)
INTERFACE_NAME=${1:-eth1}
TEST_RUN=${TEST_RUN:-false}
DEBUG=${DEBUG:-false}

OUTPUT_PREFIX="<<< TTL Shell >>>>>>> "

if [[ ${TEST_RUN} == true ]]; then
    echo "${OUTPUT_PREFIX} Running in test mode"
    ./test/run-dhcp-test.sh $1 &
    PID=$!
else
    echo "${OUTPUT_PREFIX} Running in production mode"
    ./run-dhcp.sh $INTERFACE_NAME &
    PID=$!
fi

echo "${OUTPUT_PREFIX} Captured PID = ${PID}"

i=$TTL
while [[ $i -gt 0 ]]; do
    sleep 1 # sleep for 1 second
    i=$((i-1))
    if [[ ${DEBUG} == true ]]; then
        echo "${OUTPUT_PREFIX} ${TTL} -> $i seconds left"
    fi
done

echo "${OUTPUT_PREFIX} Killing ${PID}"
kill -9 $PID
RUNNING_PID=$(ps | grep $PID | awk '{print $2}')

OOC=10 # Just in case, give a chance to break out of the loop after N-number of attempts to kill the other process
while [[ ! -z "${RUNNING_PID}" ]] && [[ ${OOC} -gt 0 ]]; do
    echo "${OUTPUT_PREFIX} ${RUNNING_PID} is still running, trying to kill"
    RUNNING_PID=$(ps | grep $PID | awk '{print $2}')
    OOC=$(($OOC-1))
done

exit 0 # exit gracefully