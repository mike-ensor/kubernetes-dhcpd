#!/bin/bash

if [[ ! -z "${DEBUG}" ]]; then
    set -x
fi

VERSION="$1"

APP_NAME="dhcp-kubernetes"

echo -e "\nBuilding and pushing ${APP_NAME}:${VERSION} to ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP_NAME}:${VERSION}"

echo ""
read -p "Are you ready to proceed? (y/N): " proceed

if [[ "${proceed}" =~ ^([yY][eE][sS]|[yY])$ ]]; then

    docker build -t ${APP_NAME}:${VERSION} .
    if [[ $? -ne 0 ]]; then
        echo "Build failed Docker"
        exit 1
    fi

    docker tag ${APP_NAME}:${VERSION} ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP_NAME}:${VERSION}
    if [[ $? -ne 0 ]]; then
        echo "Build failed to retag VERSION with upstream repo"
        exit 1
    fi

    docker tag ${APP_NAME}:${VERSION} ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP_NAME}:latest
    if [[ $? -ne 0 ]]; then
        echo "Build failed to retag LATEST with upstream repo"
        exit 1
    fi

    docker push ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP_NAME}:${VERSION}
    if [[ $? -ne 0 ]]; then
        echo "Build failed to push to to docker repository with version tag"
        exit 1
    fi

    docker push ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP_NAME}:latest
    if [[ $? -ne 0 ]]; then
        echo "Build failed to push to to docker repository with latest tag"
        exit 1
    fi
else
    echo "Aborting"
    exit 1
fi
